#include <stdlib.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <string.h>
#include <unistd.h>
#include <netdb.h>
#include <inttypes.h>

#define busy_udp 1
#define idle_udp 0
#define YES 1
#define NO 0
#define WAIT_JOIN 0
#define READY 1
#define IN_RING 2
#define IDLE 0
#define BUSY 1
#define NOLEAVING 0
#define LEAVING 1


typedef struct _server Server;
Server* setup( char **);
void full_message(char *, Server *, char * );
void chat(char *, int, struct sockaddr_in, char *);
void link2ring(Server *, int, int, struct sockaddr_in *, struct sockaddr_in *);
void mirror(int , int, struct sockaddr_in *, Server *);
