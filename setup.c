#include "setup.h"
#include "tcp.h"

/* Estrutura Servidor*/
struct _server{

int service_number;
int is_start, is_ds, is_leaving;
int state_tcp, state_udp;
int available;
int ring_available;
unsigned int own_ID;
unsigned int next_server_ID;
struct in_addr own_server_IP;
struct in_addr central_IP;
struct in_addr next_server_IP;
uint16_t own_server_PTCP;
uint16_t next_server_PTCP;
uint16_t own_server_PUDP;
uint16_t central_UDP;

};

/*  setup() */
/*  Initialize server with characteristics asked in terminal  */
Server* setup(char *argv[])
{
  Server *server = (Server *)malloc(sizeof(Server));
  struct hostent *h;
  struct in_addr *a;

  if(strcmp(argv[1],"-n")!=0 || strcmp(argv[3],"-j")!=0 || strcmp(argv[5],"-u")!=0 || strcmp(argv[7],"-t")!=0){
      printf("Invalid option\n"); exit(0);
  }
  server->is_leaving=0;
  server->available = 1;
  server->ring_available = 1;
  server->state_tcp = WAIT_JOIN;
  server->state_udp = IDLE;
  server->service_number = 43;
  server->own_ID= atoi(argv[2]);
  inet_aton(argv[4],&(server->own_server_IP));
  server->own_server_PUDP=htons(atoi(argv[6]));
  server->own_server_PTCP=htons(atoi(argv[8]));
  if((argv[9]!=NULL)&&(argv[10]!=NULL)){
    inet_aton(argv[10],&(server->central_IP));
  } else {
    if((h=gethostbyname("tejo.tecnico.ulisboa.pt"))==NULL)exit(1);
    a=(struct in_addr*)h->h_addr_list[0];
    server->central_IP = *a;
    printf("internet address: %s (%08lX)\n",inet_ntoa(*a),(long unsigned int)ntohl(a->s_addr));
  }
  if((argv[11]!=NULL)&&(argv[12]!=NULL)){
    server->central_UDP=htons(atoi(argv[12]));
  } else {
    server->central_UDP=htons(59000);
  }
return server;
}


/*  full_message()  */
/*  Produces the string that is the token to send to the central server*/
/*  msg -> specifies the request  */
/*  f_msg -> pointer to the resulting token string*/
void full_message(char * msg, Server * server, char * f_msg){

  char aux_sn[5]={0};
  char aux_own_ID[5]={0};
  char *aux_ip;
  char aux_own_ptcp[50]={0};
  char aux_own_pudp[50]={0};

  sprintf(aux_sn,"%d", server->service_number);
  sprintf(aux_own_ID,"%d", server->own_ID);
  aux_ip=inet_ntoa(server->own_server_IP);
  sprintf(aux_own_ptcp,"%d", ntohs(server->own_server_PTCP));
  sprintf(aux_own_pudp,"%d", ntohs(server->own_server_PUDP));


  if(strcmp("GET_START", msg)==0)
  {

    snprintf(f_msg, sizeof(char)* 128, "%s %s;%s", msg, aux_sn, aux_own_ID);
  }
  if(strcmp("SET_START", msg)==0)
  {

    snprintf(f_msg, sizeof(char)* 128, "%s %s;%s;%s;%s", msg, aux_sn, aux_own_ID, aux_ip, aux_own_ptcp);
  }
  if(strcmp("SET_DS", msg)==0)
  {

    snprintf(f_msg, sizeof(char)* 128, "%s %s;%s;%s;%s", msg, aux_sn, aux_own_ID, aux_ip, aux_own_pudp);
  }
  if(strcmp("WITHDRAW_START", msg)==0)
  {

    snprintf(f_msg, sizeof(char)* 128, "%s %s;%s", msg, aux_sn, aux_own_ID);
  }
  if(strcmp("WITHDRAW_DS", msg)==0)
  {

    snprintf(f_msg, sizeof(char)* 128, "%s %s;%s", msg, aux_sn, aux_own_ID);
  }

}

/*  chat()  */
/*  Handles interactions with the central server  */
/*  sendmsg -> pointer to the string that will be sent to central server  */
/*  buffer -> pointer to the string retrieved   */
/*  fd -> file descriptor of UDP connection*/
void chat(char *sendmsg, int fd, struct sockaddr_in addr, char *buffer){
  int n;
  socklen_t addrlen;

  addrlen = sizeof(addr);

  n = sendto(fd, sendmsg, strlen(sendmsg), 0, (struct sockaddr*)&addr, addrlen); ///////
  if(n==-1){printf("Error: %s\n", strerror(errno));exit(1);} // error
  printf("connect\n");
  n = recvfrom(fd, buffer, 128, 0, (struct sockaddr*)&addr, &addrlen);
  if(n==-1){printf("Error: %s\n", strerror(errno));exit(1);}

  write(1, "Central: ", 9);
  write(1, buffer, n);
  printf("\n");
}

/*  link2ring() */
/*  Start comunicationg with the central server to lear about the ring and to join it */
/*  Sets as start server and as dispatch server if necessary or connects to the start server */
/*  fd_tcp -> file descriptor to send right away the NEW token to the start server  */
/*  fd_udp -> file descriptor to comunicate with the central server */
void link2ring(Server *server, int fd_tcp, int fd_udp, struct sockaddr_in *addr_tcp, struct sockaddr_in *addr_udp)
{
  int n;
  char buffer[128]={0};
  char f_msg[128]={0};
  char nobody_there[400]={0};
  char *ptr, *str;
  char *arranque_ip, aux_ID[5] = {0};
  int nbytes, nwritten, nleft;


  full_message("GET_START",server, f_msg);
  chat(f_msg, fd_udp, *addr_udp, buffer);

  sprintf(aux_ID, "%d", server->own_ID);
  snprintf(nobody_there, 400, "OK %s;0;0.0.0.0;0", aux_ID);

  if(strcmp(nobody_there,buffer) == 0){
      // Inicialize as Arranque server
      full_message("SET_START",server, f_msg);
      chat(f_msg, fd_udp, *addr_udp, buffer);
    if (strcmp(nobody_there,buffer) == 0){
      server->is_start = 1;
      //Inicialize as Despache server
        full_message("SET_DS",server, f_msg);
        chat(f_msg, fd_udp, *addr_udp, buffer);
      if (strcmp(nobody_there,buffer) == 0){
        server->is_ds = 1;
        printf("Server set as START and DS\n");
        // Link Arranque server to himself
        server->next_server_ID=server->own_ID;
        server->next_server_IP=server->own_server_IP;
        server->next_server_PTCP=server->own_server_PTCP;
      }
    } else {printf("Cannot retrieve START server\n"); exit(0);}
  }else{

    server->is_start = 0;
    server->is_ds = 0;
    // Geting ip and p_tcp of arranque server
    printf("CONNECTING TO ARRANQUE...\n");
    str = strtok(buffer, " ;");
    str = strtok(NULL, " ;");
    str = strtok(NULL, " ;");
    printf("next_server_ID %s\n",str );
    server->next_server_ID = atoi(str);
    str = strtok(NULL, " ;");
    printf("next_server_IP %s\n",str );
    arranque_ip = str;
    inet_aton(arranque_ip,&(server->next_server_IP));
    str = strtok(NULL, " ;");
    printf("next_server_PTCP %d\n",atoi(str) );
    server->next_server_PTCP = htons(atoi(str));

    // TCP socket info refresh
    addr_tcp->sin_addr=server->next_server_IP;
    addr_tcp->sin_port=server->next_server_PTCP;

    n=connect(fd_tcp,(struct sockaddr*) addr_tcp,sizeof(*addr_tcp));
    if(n==-1){printf("Error: %s\n", strerror(errno));exit(1);}//error

    // Manda o token NEW ao servirdor de arranque sobre a nossa entrada no anel
    tokens(f_msg, "NEW", "NO TYPE", server, "NO STR", "NO STR", "NO STR");
    ptr=f_msg;

    nbytes=strlen(ptr);
    nleft=nbytes;
    // Send menssege to Arranque server
    while(nleft>0){
      nwritten=write(fd_tcp,ptr,nleft);
      if(nwritten<=0){printf("Error: %s\n", strerror(errno));exit(1);}//error
        nleft-=nwritten;
        ptr+=nwritten;
     }
    nleft=nbytes;
   }
   ///////METE-SE COMO READY e idle////////
   server->state_tcp = READY;
   server->state_udp = IDLE;
}



/*  mirror()  */
/*  Gets request from clients and responds accordingly  */
/*  fd_udp -> file descriptor to comunicate with the client  */
/*  fd_tcp -> file descriptor to send S token if the server beggins providing a service to a client  */

void mirror(int fd_udp, int fd_tcp, struct sockaddr_in *addr, Server *server){

socklen_t  addrlen;
ssize_t n, nw;
char buffer[128]={0}, aux_msg[128]={0}, *ptr;

addrlen=sizeof(addr);
// recebe mensagem do cliente
addrlen = sizeof(*addr);
n=recvfrom(fd_udp,buffer,128,0,(struct sockaddr*)addr,&addrlen);
if(n==-1)
{
  printf("Error: %s\n", strerror(errno));
  exit(1);
}
if(server->state_udp == IDLE)
{
  if(strcmp("MY_SERVICE ON", buffer)==0)
  {
    tokens(aux_msg, "TOKEN", "S", server, "NO STR", "NO STR", "NO STR");
    ptr = aux_msg;
    n = strlen(aux_msg);
    while(n>0){if((nw=write(fd_tcp,ptr,n))<=0){printf("Error: %s\n", strerror(errno));exit(1);}//error
    n-=nw; ptr+=nw;}

    n=sendto(fd_udp,"YOUR_SERVICE ON",15,0,(struct sockaddr*)addr,addrlen);
    if(n==-1){  printf("Error: %s\n", strerror(errno)); exit(1);}//error
    server->state_udp = BUSY;
  }
}
if(server->state_udp == BUSY)
{
  // Não te vou dar o meu serviço
  if(strcmp("MY_SERVICE OFF", buffer)==0)
  {
    n=sendto(fd_udp,"YOUR_SERVICE OFF",16,0,(struct sockaddr*)addr,addrlen);
      if(n==-1){  printf("Error: %s\n", strerror(errno)); exit(1);}//error
      server->state_udp = IDLE;
  }
  if(server->state_udp == BUSY)
  {
    // Não te vou dar o meu serviço
    if(strcmp("MY_SERVICE OFF", buffer)==0)
    {
      n=sendto(fd_udp,"YOUR_SERVICE OFF",16,0,(struct sockaddr*)addr,addrlen);
        if(n==-1){  printf("Error: %s\n", strerror(errno)); exit(1);}//error
        server->state_udp = IDLE;
    }
  }

}
}
