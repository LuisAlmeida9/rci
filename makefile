.PHONY: run
run: service
	./service -n 1 -j 10.0.2.15 -u 58000 -t 59000 -i 193.136.138.142 -p 59000

service: service.o setup.o tcp.o
	gcc -g -o service service.o setup.o tcp.o

service.o: service.c setup.h
	gcc -g -c service.c

setup.o: setup.c tcp.c setup.h
	gcc -g -c setup.c

tcp.o: tcp.c tcp.h setup.c
	gcc -g -c tcp.c

clean:
	rm service service.o setup.o tcp.o
