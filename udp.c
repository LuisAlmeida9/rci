#include "udp.h"

// Read the ip and port from command line
void setup(char * cpip,int * cspt, char *argv[]){
  struct hostent *h;
  struct in_addr *a;
  // O ip e o porto do cliente são os provenientes da linha de comandos
    if (argv[1] != NULL && argv[3] != NULL){

      strcpy((cpip),argv[2]);
      (*cspt)=atoi(argv[4]);
    }else{
      //Caso contrario, são o porto e o ip do servidor central
      if((h=gethostbyname("tejo.tecnico.ulisboa.pt"))==NULL)exit(1);
      a=(struct in_addr*)h->h_addr_list[0];
      strcpy((cpip),inet_ntoa(*a));
      (*cspt)=59000;
    }
}

// Esta função tem os parametros necessarios a transmissao e resposta de uma mensagem
// enviada por UDP
void chat(char *sendmsg,int fd, struct sockaddr_in addr, char *buffer){
  int n;
  socklen_t addrlen;

  n = sendto(fd, sendmsg, strlen(sendmsg), 0, (struct sockaddr*)&addr, sizeof(addr));
  if(n==-1){printf("Error: %s\n", strerror(errno));exit(1);} // error

  addrlen = sizeof(addr);
  n = recvfrom(fd, buffer, 128, 0, (struct sockaddr*)&addr, &addrlen);
  if(n==-1){printf("Error: %s\n", strerror(errno));exit(1);}

  write(1, "Server: ", 8);
  write(1, buffer, n);
  printf("\n");
}


struct sockaddr_in  whichserverisready(char * buff, int fd, struct sockaddr_in addr,struct sockaddr_in despacho ){
char f_msg[400]={0};
char sendmsg[400]={0};
char read_msg[400]={0};
struct in_addr server_IP;// Endereço IP do servidor que vai satisfazer o pedido
int service;
char buffer[400]={0};
int indice;
char ip[50]={0};
char porto[50]={0}, *str;

  // Se o cliente quer um serviço, tem que mandar mensagem ao servidor central,
  // de forma a obter o porto e ip do servidor de despacho
  if(strncmp("request_service", buff,15)==0){
    sscanf(buff, "request_service %d",&service);
    snprintf(f_msg, sizeof(char)* 400, "GET_DS_SERVER %d",service);
    chat(buff,fd,  addr, buffer);
    printf("buffer %s\n", buffer);
    // obtem endereço IP e o Porto do servidor de despacho a partir da mensagem
    // proveniente do servidor central
    str = strtok(buffer, " ;");
    str = strtok(NULL, " ;");
    indice = atoi(str);
    str = strtok(NULL, " ;");
    strcpy(ip, str);
    str = strtok(NULL, " ;");
    strcpy(porto,str);

    // Establecer ligação com o servidor e pedir o seu serviço
    inet_aton(ip,&(server_IP));
    memset((void*)&despacho, (int)'\0', sizeof(despacho));
    despacho.sin_family = AF_INET;
    despacho.sin_addr = server_IP;
    despacho.sin_port = atoi(porto);
    // caso nao haja servidor de despacho
    if(strcmp(porto, "0")==0)// Não há servidor de despacho
    {
      printf("Não há servidor de despacho! Tente mais tarde\n");
      return despacho;
    }


    // Envia para o servidor a mensagem MY_SERVICE ON
    strcpy(sendmsg,"MY_SERVICE ON");
    chat(sendmsg, fd, despacho, read_msg);
    if(strcmp(read_msg, "YOUR_SERVICE ON")!=0)
    {
      printf("Couldn't connect with server! Try again\n");
      return despacho;
    }
  }
  //REQUEST_SERVICE (a mesma coisa que acontece no request_service)
  if(strncmp("rs", buff,2)==0){
    sscanf(buff, "rs %d",&service);
    snprintf(f_msg, sizeof(char)* 400, "GET_DS_SERVER %d",service);
    chat(f_msg,fd,  addr, buffer);
    printf("buffer %s\n", buffer);


    str = strtok(buffer, " ;");
    str = strtok(NULL, " ;");
    indice = atoi(str);
    str = strtok(NULL, " ;");
    strcpy(ip, str);
    str = strtok(NULL, " ;");
    strcpy(porto,str);

    // Establecer ligação com o servidor e pedir o seu serviço
    inet_aton(ip,&(server_IP));
    memset((void*)&despacho, (int)'\0', sizeof(despacho));
    despacho.sin_family = AF_INET;
    despacho.sin_addr = server_IP;
    despacho.sin_port = htons(58000);
    if(strcmp(porto, "0")==0)// Não há servidor de despacho
    {
      printf("Não há servidor de despacho! Tente mais tarde\n");
      return despacho;
    }


    // Envia para o servidor a mensagem MY_SERVICE ON
    strcpy(sendmsg,"MY_SERVICE ON");
    chat(sendmsg, fd, despacho, read_msg);
    if(strcmp(read_msg, "YOUR_SERVICE ON")!=0)
    {
      printf("Couldn't connect with server! Try again\n");
      return despacho;
    }else printf("I am getting the service\n" );
   return despacho;
  }
  // TERMINATE SERVICE!!!!!
  // Manda uma mensagem ao servidor de despacho a dizer que nao precisa mais do serviço
    if(strncmp("terminate_service", buff,17)==0 || strncmp("ts", buff,2)==0){
        // Ve se está ligado ao servidor de despacho
        if(ntohs(despacho.sin_port)==0)
        {
          printf("Está a tentar fechar um serviço com ninguem...\n" );
          return despacho;
        }
        strcpy(sendmsg,"MY_SERVICE OFF");
        chat(sendmsg, fd, despacho, read_msg);
        if(strcmp(read_msg, "YOUR_SERVICE OFF")!=0)
        {
          printf("Couldn't connect with server! Try again\n");
          return despacho;
        }
        return despacho;

    }

}

void printes(){
printf("========================================\n\n\n" );
printf("Introduza os seguintes comandos para    \n" );
printf("pedir ou terminar o serviço pretendido  \n" );
printf("ou sair do programa.                    \n\n\n " );
printf("Solicitação de um serviço x:            \n" );
printf("-> request_service x (abreviatura rs x) \n\n" );
printf("Conclusão do serviço em curso:          \n" );
printf("-> terminate_service (abreviatura ts)   \n\n" );
printf("Terminação da aplicação.                \n" );
printf("-> exit                                 \n\n" );

}
