#include "setup.h"
#include "tcp.h"
#include <signal.h>
#define max(A,B) ((A)>=(B)?(A):(B))

/* Estrutura Servidor*/
//is_start, is_sd -> flags to determine: is this server the start server?; is this the dispatch server_IP?
//state_tcp, state_udp -> flags to pass the server's state to other functions
//available, ring_available -> flags to determine: is this server available for an user?; is the ring available?

struct _server{

int service_number;
int is_start, is_ds, is_leaving;
int state_tcp, state_udp;
int available, ring_available;
unsigned int own_ID, next_server_ID;
struct in_addr own_server_IP;
struct in_addr central_IP;
struct in_addr next_server_IP;
uint16_t own_server_PTCP;
uint16_t next_server_PTCP;
uint16_t own_server_PUDP;
uint16_t central_UDP;

};

int main(int argc, char *argv[]){

  int fd = 0, newfd = 0, afd_tcp = 0, afd_udp = 0, fd_tcp = 0, true, ret;
  fd_set rfds;
  socklen_t addrlen;
  struct sockaddr_in addr, *addr_tcp = (struct sockaddr_in *)malloc(sizeof(struct sockaddr_in)), *addr_udp = (struct sockaddr_in *)malloc(sizeof(struct sockaddr_in));
  struct sockaddr_in addr_udp_l;
  int n, nw;
  char *ptr, *buffer2, aux_ID[5] = {0}, aux_PTCP[50] = {0}, aux_msg[100]={0}, buffer[128]={0}, buff[255] = {0}, message[255] = {0};
  //'wait_join' represents a state in wich the server is only able to join the ring or exit the programa i.e. read commands from the user
  //'ready' represents a state in wich the server is waiting for a connection from his future predecessor
  //'in_ring' is the state where the server is currently part of the ring, able to read from the connection with his predecessor
  //'idle' is a state in wich the server is free to start answering to a new cliente
  //'busy' is the state where the server is occupied with a user
  enum {idle,busy} state_udp;
  int maxfd, counter;
  int len;

  Server *server = setup( argv);

  if(argc<=8){
    printf("Insuficient arguments\n");
    exit(1);
  }

  ///////////////////////////////////// UDP /////////////////////////////////////
  // UDP socket
  afd_udp = socket(AF_INET, SOCK_DGRAM, 0); //UDP socket
  if(afd_udp==-1) {printf("Error: %s\n", strerror(errno));exit(1);} //error

  //struct sockaddr_in to handle linstening in a binded udp port
  memset((void*)&addr_udp_l, (int)'\0', sizeof(addr_udp_l));
  addr_udp_l.sin_family = AF_INET;
  addr_udp_l.sin_addr.s_addr = htonl(INADDR_ANY);
  addr_udp_l.sin_port = server->own_server_PUDP;

  ret=bind(afd_udp,(struct sockaddr*)&addr_udp_l,sizeof(addr_udp_l));
  if(ret==-1){printf("Error: %s\n", strerror(errno));exit(1);}//error

  //struct sockaddr_in to handle message sending to an udp port
  memset((void*)addr_udp, (int)'\0', sizeof(*addr_udp));
  addr_udp->sin_family = AF_INET;
  addr_udp->sin_addr = server->central_IP;
  addr_udp->sin_port = server->central_UDP;

  ///////////////////////////////////// TCP /////////////////////////////////////
  //TCP socket
  if((fd=socket(AF_INET,SOCK_STREAM,0))==-1)  {printf("Error: %s\n", strerror(errno));exit(2);}//error
  //struct sockaddr_in to handle listening in a binded tcp port
  memset((void*)&addr,(int)'\0',sizeof(addr));
  addr.sin_family=AF_INET;
  addr.sin_addr.s_addr=htonl(INADDR_ANY);
  addr.sin_port=server->own_server_PTCP;

  if(bind(fd,(struct sockaddr*)&addr,sizeof(addr))==-1){printf("Error: %s\n", strerror(errno));exit(1);}//error
  if(listen(fd,5)==-1){printf("Error: %s\n", strerror(errno));exit(4);}//error

  //TCP socket
  if((fd_tcp=socket(AF_INET,SOCK_STREAM,0))==-1)  {printf("Error: %s\n", strerror(errno));exit(2);}//error
  //struct sockaddr_in to handle message sending to a tcp port
  memset((void*)addr_tcp,(int)'\0',sizeof(*addr_tcp));
  addr_tcp->sin_family=AF_INET;
  addr_tcp->sin_addr=server->own_server_IP;
  addr_tcp->sin_port=server->own_server_PTCP;

  ///////////////////////////////////////////////////////////////////////////////

  //Set the state variables to handle actions from the terminal only
  server->state_tcp = WAIT_JOIN;

  //While in here, the server will be able to join the service until and exit call
  while(1){

    //Reset to NULL the memory in the auxiliar strings
    memset(buffer, '\0', sizeof(buffer));
    memset(buff, '\0', sizeof(buff));
    memset(aux_msg, '\0', sizeof(aux_msg));

    //Reset the memory keeping the file descriptors and set the one to handle STDIN
    FD_ZERO(&rfds);
    FD_SET(STDIN_FILENO,&rfds); maxfd = 0;

    if(server->state_tcp!=WAIT_JOIN)
    {
      //Set TCP file descriptor for new connections
      FD_SET(fd,&rfds);maxfd=max(maxfd,fd);
      if(server->state_tcp == IN_RING)
      {
        //Set TCP file descriptor to handle a specific connection
        FD_SET(afd_tcp,&rfds);maxfd=max(maxfd,afd_tcp);
      }
      //Set file descriptor to handle UDP comunications
      FD_SET(afd_udp,&rfds);maxfd=max(maxfd,afd_udp);
    }


    counter=select(maxfd+1,&rfds,(fd_set*)NULL,(fd_set*)NULL,(struct timeval *)NULL);
    if(counter<=0){printf("Error: %s\n", strerror(errno));exit(1);}//error


    //Assures that, when out of the ring, no file descriptors are checked if SET except the STDIN (take input from user only)
    if(server->state_tcp!=WAIT_JOIN)
    {
      //Check if there is a new connection request
      if(FD_ISSET(fd,&rfds))
      {
        addrlen=sizeof(addr);
        if((newfd=accept(fd,(struct sockaddr*)&addr,&addrlen))==-1){printf("Error: %s\n", strerror(errno));exit(1);}//error
        printf("ACCEPT\n");

        switch(server->state_tcp)
        {
          //Nothing to be done, just to assure nothing does happen
          case WAIT_JOIN : break;
          //Kepp the new file descriptor to handle the new connection
          case READY: afd_tcp=newfd;  server->state_tcp = IN_RING; break;
          //Kepp the new file descriptor to handle the new connection
          case IN_RING : afd_tcp=newfd; break;
        }

      }

      // If the server is in the ring, check for activity in the connection with the predecessor
      if(server->state_tcp == IN_RING )
      {
        if(FD_ISSET(afd_tcp,&rfds))
        {
          if((n=read(afd_tcp,buffer,128))!=0){if(n==-1){printf("Error: %s\n", strerror(errno));exit(1);}//error
          ptr=&buffer[0];
          while(n>0){if((nw=write(1,ptr,n))<=0){printf("Error: %s\n", strerror(errno));exit(1);}//error
          n-=nw; ptr+=nw;}
          }

          buffer2 = strtok(buffer,"\n");

          while(buffer2 != NULL){

            //Processes the message received
            get_token(buffer2, server, &fd_tcp, &afd_udp, addr_tcp, addr_udp);
            buffer2 = strtok(NULL,"\n");
          }
          //Update TCP state variables with possible changes depending on the message received
          switch(server->state_tcp)
          {
            case WAIT_JOIN: server->state_tcp = WAIT_JOIN; break;
            case READY: server->state_tcp = READY; break;
            case IN_RING: server->state_tcp = IN_RING ; break;
          }
        }
      }

      //Checks for activity in the UDP connection
      if(FD_ISSET(afd_udp,&rfds))
      {
        mirror(afd_udp, afd_tcp, addr_udp, server);

        //Update UDP state variables from possible changes due to user requests
        switch(server->state_udp)
        {
          case IDLE: server->state_udp = IDLE; break;
          case BUSY: server->state_udp = BUSY; break;
        }

      }

    }




    //Handles commands from the terminal
    if(FD_ISSET(STDIN_FILENO,&rfds))
    {

      fgets(buff, sizeof(buff), stdin);

      //Remove trailing newline character from the input buffer if needed.
      len = strlen(buff) - 1;
      if (buff[len] == '\n')
      buff[len] = '\0';

      //Check for join service from user (not complete since user can only join service 43)
      if(strcmp("join 43", buff)==0)
      {
        //Assure that join call is only valid if the server is not in the ring already
        if(server->state_tcp == WAIT_JOIN)
        {
          link2ring(server, fd_tcp, afd_udp, addr_tcp, addr_udp);
          server->state_tcp = READY;
          server->available=1;
        }
      }
      if(strcmp("show_state", buff)==0 && server->state_tcp == IN_RING)
      {
        printf("SUCESSOR: %d\n",server->next_server_ID);
        printf("DISPONIBILIDADE: %d\n",server->available);
        printf("DISPONIBILIDADE DO ANEL: %d\n",server->ring_available);
      }
      if(strcmp("leave", buff)==0 && server->state_tcp == IN_RING)
      {
        sprintf(aux_ID,"%d", server->next_server_ID);
        sprintf(aux_PTCP,"%d", ntohs(server->next_server_PTCP));

        if(server->is_ds)
        {
          full_message("WITHDRAW_DS",server, message);
          chat(message, afd_udp, *addr_udp, buffer);

          tokens(aux_msg, "TOKEN", "S", server, aux_ID, inet_ntoa(server->next_server_IP), aux_PTCP);
          ptr = aux_msg;
        n = strlen(aux_msg);
        while(n>0){if((nw=write(fd_tcp,ptr,n))<=0){printf("Error: %s\n", strerror(errno));exit(1);}//error
          n-=nw; ptr+=nw;}

          server->is_ds = 0;
          server->is_leaving=LEAVING;
        }

        if(server->is_start)
        {
          full_message("WITHDRAW_START",server, message);
          chat(message, afd_udp, *addr_udp, buffer);
          tokens(aux_msg, "NEW_START", "STUFF", server, aux_ID, inet_ntoa(server->next_server_IP), aux_PTCP);
          ptr = aux_msg;
          n = strlen(aux_msg);
          while(n>0){if((nw=write(fd_tcp,ptr,n))<=0){printf("Error: %s\n", strerror(errno));exit(1);}//error
            n-=nw; ptr+=nw;}
          server->is_start = 0;
        }

        //Se o servidor nao for o de despacho manda logo o token
        if (server->is_leaving==NOLEAVING)
        {
          sprintf(aux_ID,"%d", ntohs(server->next_server_ID));
          sprintf(aux_PTCP,"%d", ntohs(server->next_server_PTCP));
          tokens(aux_msg, "TOKEN", "O", server, aux_ID, inet_ntoa(server->next_server_IP), aux_PTCP);
          ptr = aux_msg;
          printf("LEaving when i am not  a DS or SS: %s\n", ptr);
          n = strlen(aux_msg);
          while(n>0){if((nw=write(fd_tcp,ptr,n))<=0){printf("Error: %s\n", strerror(errno));exit(1);}//error
          n-=nw; ptr+=nw;}
          server->state_tcp = WAIT_JOIN;
        }

        //Tells client that service will be shut down due to the end of the server program
      /*  n=sendto(afd_udp,"YOUR_SERVICE OFF",16,0,(struct sockaddr*)addr_udp,sizeof(*addr_udp));
          if(n==-1){  printf("Error: %s\n", strerror(errno)); exit(1);}//error*/


      }
      if(strcmp("leave", buff)==0 && server->state_tcp == READY)
      {
        full_message("WITHDRAW_DS",server, message);
        chat(message, afd_udp, *addr_udp, buffer);
        full_message("WITHDRAW_START",server, message);
        chat(message, afd_udp, *addr_udp, buffer);

        server->state_tcp = WAIT_JOIN;
      }

      //Exits the program after terminating all connections
      if(strcmp("exit", buff)==0 && server->state_tcp == IN_RING)
      {
        printf("I am leaving\n" );
        sprintf(aux_ID,"%d", ntohs(server->next_server_ID));
        sprintf(aux_PTCP,"%d", ntohs(server->next_server_PTCP));
        tokens(aux_msg, "TOKEN", "O", server, aux_ID, inet_ntoa(server->own_server_IP), aux_PTCP);
        n = strlen(aux_msg);
        while(n>0){if((nw=write(afd_tcp,ptr,n))<=0){printf("Error: %s\n", strerror(errno));exit(1);}//error
        n-=nw; ptr+=nw;}

        if(server->is_ds)
        {
          full_message("WITHDRAW_DS",server, message);
          chat(message,  afd_udp, *addr_udp, buffer);
        }
        if(server->is_start)
        {
          full_message("WITHDRAW_START",server, message);
          chat(message, afd_udp, *addr_udp, buffer);
        }
        true = 1;
        setsockopt(fd,SOL_SOCKET,SO_REUSEADDR,&true,sizeof(int));
        close(fd);
        setsockopt(fd_tcp,SOL_SOCKET,SO_REUSEADDR,&true,sizeof(int));
        close(fd_tcp);
        printf("Fechou\n");
        break;
      }
      //Exit when not in ring
      if(strcmp("exit", buff)==0 && server->state_tcp == READY)
      {
        if(server->is_ds)
        {
          full_message("WITHDRAW_DS",server, message);
          chat(message, afd_udp, *addr_udp, buffer);
        }
        if(server->is_start)
        {
          full_message("WITHDRAW_START",server, message);
          chat(message, afd_udp, *addr_udp, buffer);
        }

        server->state_tcp = WAIT_JOIN;

        true = 1;
        setsockopt(fd,SOL_SOCKET,SO_REUSEADDR,&true,sizeof(int));
        close(fd);
        setsockopt(fd_tcp,SOL_SOCKET,SO_REUSEADDR,&true,sizeof(int));
        close(fd_tcp);
        printf("Fechou\n");
        break;
      }
      // Exit when in wait_join
      if(strcmp("exit", buff)==0 && server->state_tcp == WAIT_JOIN)
      {

        true = 1;
        setsockopt(fd,SOL_SOCKET,SO_REUSEADDR,&true,sizeof(int));
        close(fd);
        setsockopt(fd_tcp,SOL_SOCKET,SO_REUSEADDR,&true,sizeof(int));
        close(fd_tcp);
        printf("Fechou\n");
        break;
      }


    }

  }

    free(server);

}

//./service -n 2 -j 194.210.134.42 -u 56000 -t 57000 -i 193.136.138.142 -p 59000
//./service -n 3 -j 194.210.134.42 -u 54000 -t 55000 -i 193.136.138.142 -p 59000
//./service -n 4 -j 194.210.134.42 -u 52000 -t 53000 -i 193.136.138.142 -p 59000
//./service -n 5 -j 194.210.134.42 -u 50000 -t 51000 -i 193.136.138.142 -p 59000
//./service -n 6 -j 194.210.134.42 -u 48000 -t 49000 -i 193.136.138.142 -p 59000
