#include <stdlib.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <string.h>
#include <unistd.h>
#include <netdb.h>
#include <errno.h>


typedef struct _server Server;
void get_token(char *, Server *, int *, int *, struct sockaddr_in *, struct sockaddr_in *);
void tokens(char *, char *, char *, Server *, char *, char *, char *);
