#include <stdlib.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <string.h>
#include <unistd.h>
#include <stdio.h>
#include <netdb.h>

#define max(A,B) ((A)>=(B)?(A):(B))

/* Estrutura Servidor*/
struct _server{

int service_number;
unsigned int own_ID;
unsigned int next_server_ID;
struct in_addr own_server_IP;
struct in_addr central_IP;
struct in_addr next_server_IP;
uint16_t own_server_PTCP;
uint16_t next_server_PTCP;
uint16_t own_server_PUDP;
uint16_t central_UDP;

};


int main(void)
{
int fd, newfd, afd;
fd_set rfds;
socklen_t addrlen;
struct sockaddr_in addr;
int n, nw;
char *ptr, buffer[128];
enum {idle,busy} state;
int maxfd, counter;

if((fd=socket(AF_INET,SOCK_STREAM,0))==-1)  exit(1);//error

memset((void*)&addr,(int)'\0',sizeof(addr));
addr.sin_family=AF_INET;
addr.sin_addr.s_addr=htonl(INADDR_ANY);
addr.sin_port=htons(9000);

if(bind(fd,(struct sockaddr*)&addr,sizeof(addr))==-1)
  exit(1);//error

if(listen(fd,5)==-1)exit(1);//error

state=idle;

while(1){FD_ZERO(&rfds);
  FD_SET(fd,&rfds);maxfd=fd;
  if(state==busy){FD_SET(afd,&rfds);maxfd=max(maxfd,afd);}

counter=select(maxfd+1,&rfds,(fd_set*)NULL,(fd_set*)NULL,(struct timeval *)NULL);

if(counter<=0)exit(1);//error

if(FD_ISSET(fd,&rfds))
{
  addrlen=sizeof(addr);
  if((newfd=accept(fd,(struct sockaddr*)&addr,&addrlen))==-1)exit(1);//error
  switch(state)
    {
      case idle: afd=newfd; state=busy; break;
      case busy: afd=write(newfd,"busy\n",n);//write “busy\n” in newfd
    }
    close(newfd); break;
}
if(FD_ISSET(afd,&rfds))
{
  if((n=read(afd,buffer,128))!=0)
  {if(n==-1)exit(1);//error
// write buffer in afd
  }
  else{close(afd); state=idle;}//connection closed by peer }
}
}
}
