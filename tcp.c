#include "tcp.h"
#include "setup.h"

/* Estrutura Servidor*/
struct _server{

int service_number;
int is_start, is_ds, is_leaving;
int state_tcp, state_udp;
int available;
int ring_available;
unsigned int own_ID;
unsigned int next_server_ID;
struct in_addr own_server_IP;
struct in_addr central_IP;
struct in_addr next_server_IP;
uint16_t own_server_PTCP;
uint16_t next_server_PTCP;
uint16_t own_server_PUDP;
uint16_t central_UDP;

};

/*  get_token() */
/*  This function will receive a string from a TCP connection and will check all token possibilities  to be handled */
/*  and acts accordingly */
/*  The actions will be updating the next server information, passing along the token and creating and sending other tokens  */
void get_token(char *f_msg, Server *server, int *fd_tcp, int *fd_udp, struct sockaddr_in *addr_tcp, struct sockaddr_in *addr_udp)
{
  char *str, *aux_str, *aux_type, *id, *ip, *ptcp, *ptr;
  char send_msg[100] = {0}, read_msg[100] = {0}, buffer[100] = {0}, bptcp[128]={0};
  int nleft, nwritten, nbytes, n;

  // Geting ip and p_tcp of arranque server
  strcpy(read_msg,f_msg);
  str = strtok(f_msg, " ;\n");
  aux_str = str;

  ////////////////TOKEN NEW//////////////////////////
  if(strcmp("NEW", aux_str)==0)
  {
    printf("GETTING NEW SERVER...\n");
    str = strtok(NULL, " ;\n");
    id = str;
    printf("ID from NEW server: %s\n", id);
    str = strtok(NULL, " ;\n");
    ip = str;
    printf("IP from NEW server: %s\n", ip);
    str = strtok(NULL, " ;\n");
    ptcp = str;
    printf("PTCP from NEW server: %s\n", ptcp);
    // Primeiro servidor a ligar-se ao servidor de arranque_ip
    if(server->next_server_ID == server->own_ID)
    {
      printf("NOT ALONE ANYMORE!...\n");
      server->next_server_ID = atoi(id);
      inet_aton(ip,&(server->next_server_IP));
      server->next_server_PTCP = htons(atoi(ptcp));
      addr_tcp->sin_addr=server->next_server_IP;
      addr_tcp->sin_port=server->next_server_PTCP;
      n=connect(*fd_tcp,(struct sockaddr*)addr_tcp,sizeof(*addr_tcp));
      if(n==-1){printf("Error: %s\n", strerror(errno));exit(1);}//error

      server->state_tcp = IN_RING;


    }else{
      //Envia token para o anel com info do novo servidor
      printf("GET HIM IN...\n");
      tokens(send_msg, "TOKEN", "N", server, id, ip, ptcp);

      ptr=send_msg;

      nbytes=strlen(ptr);
      nleft=nbytes;
      // Send menssege to Arranque server
      while(nleft>0){
        nwritten=write(*fd_tcp,ptr,nleft);
        write(1,ptr,nleft);
        if(nwritten<=0){printf("Error: %s\n", strerror(errno));exit(1);}//error
          nleft-=nwritten;
          ptr+=nwritten;
        }
    }

  }
  ////////////////TOKEN TOKEN//////////////////////////
  if(strcmp("TOKEN", aux_str)==0)
  {
    str = strtok(NULL, " ;\n");
    id = str;
    str = strtok(NULL, " ;\n");
    aux_type = str;

    if(strcmp("N", aux_type)==0)
    {
      printf("NEW SERVER, IS IT FOR ME?...\n");
      if(server->next_server_ID == (unsigned int)atoi(id))
      {
        printf("YES!  NEW NEXT:\n");
        str = strtok(NULL, " ;\n");
        server->next_server_ID = (unsigned int)atoi(str);
        printf("next_server_ID %s\n",str );
        str = strtok(NULL, " ;\n");
        inet_aton(str,&(server->next_server_IP));
        printf("next_server_IP %s\n",str );
        str = strtok(NULL, " ;\n");
        server->next_server_PTCP = htons(atoi(str));
        printf("next_server_PTCP %s\n",str);
        addr_tcp->sin_addr=server->next_server_IP;
        addr_tcp->sin_port=server->next_server_PTCP;

        close(*fd_tcp);
        if((*fd_tcp=socket(AF_INET,SOCK_STREAM,0))==-1)  {printf("Error: %s\n", strerror(errno));exit(2);}//error
        n=connect(*fd_tcp,(struct sockaddr*)addr_tcp,sizeof(*addr_tcp));
        if(n==-1){printf("Error: %s\n", strerror(errno));exit(1);}//error



      }else{

        printf("PASS ALONG...\n");
        //Reencaminha token para o próximo servidor
        ptr=read_msg;
        nbytes=strlen(ptr);
        nleft=nbytes;
        // Send menssege to next server
        while(nleft>0){
          nwritten=write(*fd_tcp,ptr,nleft);
          if(nwritten<=0){printf("Error: %s\n", strerror(errno));exit(1);}//error
            nleft-=nwritten;
            ptr+=nwritten;
          }
        nleft=nbytes;

      }
    }
    if(strcmp("O", aux_type)==0)
    {
      //Servidor sai do anel (recebe novamente o token que mandou)
      if(server->own_ID == (unsigned int)atoi(id))
      {
        if(server->state_udp == BUSY)
        {
          close(*fd_udp);
          if((*fd_udp=socket(AF_INET, SOCK_DGRAM, 0))==-1)  {printf("Error: %s\n", strerror(errno));exit(2);}//error
        }
        printf("LEAVE OK\n");
        server->state_tcp = WAIT_JOIN;
        server->state_udp = IDLE;
        server->next_server_ID = server->own_ID;
        server->available = 0;
        server->is_leaving=NOLEAVING;
        close(*fd_tcp);
        if((*fd_tcp=socket(AF_INET,SOCK_STREAM,0))==-1)  {printf("Error: %s\n", strerror(errno));exit(2);}//error
      }else{
        printf("SOMEONE GETTING OUT, IS IT FOR ME?...\n");
        if(server->next_server_ID == (unsigned int)atoi(id))
        {
          // Eu vou me ligar ao sucessor do servidor que vai sair
          printf("YES!  NEW NEXT:\n");
          str = strtok(NULL, " ;\n");
          server->next_server_ID = atoi(str);
          printf("next_server_ID %s\n",str );
          str = strtok(NULL, " ;\n");
          inet_aton(str,&(server->next_server_IP));
          printf("next_server_IP %s\n",str );
          str = strtok(NULL, " ;\n");
          server->next_server_PTCP = htons(atoi(str));
          printf("next_server_PTCP %s\n",str);

          printf("PASS ALONG...\n");
          //Reencaminha token para o próximo servidor
          ptr=read_msg;
          nbytes=strlen(ptr);
          nleft=nbytes;
          // Send menssege to next server
          while(nleft>0){
            nwritten=write(*fd_tcp,ptr,nleft);
            if(nwritten<=0){printf("Error: %s\n", strerror(errno));exit(1);}//error
              nleft-=nwritten;
              ptr+=nwritten;
            }
          nleft=nbytes;

          addr_tcp->sin_addr=server->next_server_IP;
          addr_tcp->sin_port=server->next_server_PTCP;

          close(*fd_tcp);
          if((*fd_tcp=socket(AF_INET,SOCK_STREAM,0))==-1)  {printf("Error: %s\n", strerror(errno));exit(2);}//error
          if(server->own_ID != server->next_server_ID)
          {
            n=connect(*fd_tcp,(struct sockaddr*)addr_tcp,sizeof(*addr_tcp));
            if(n==-1){printf("Error: %s\n", strerror(errno));exit(1);}//error
          }else{
            server->state_tcp = READY;
          }


        }else{
          // O meu successor ainda não é o servidor que vai sair, vou passar ao proximo server
          printf("PASS ALONG...\n");
          //Reencaminha token para o próximo servidor
          ptr=read_msg;
          nbytes=strlen(ptr);
          nleft=nbytes;
          // Send menssege to next server
          while(nleft>0){
            nwritten=write(*fd_tcp,ptr,nleft);
            if(nwritten<=0){printf("Error: %s\n", strerror(errno));exit(1);}//error
              nleft-=nwritten;
              ptr+=nwritten;
            }
          nleft=nbytes;

        }
      }

    }
    if(strcmp("S", aux_type)==0)
    {
      if(server->own_ID != (unsigned int)atoi(id))
      {
        if(server->available)
        {
          printf("I AM AVAILABLE...\n");

          full_message("SET_DS",server, send_msg);
          chat(send_msg, *fd_udp, *addr_udp, buffer);
          server->is_ds = 1;

          //Envia TOKEN T para o próximo servidor
          tokens(send_msg, "TOKEN", "T", server, id, "NO IP", "NO PTCP");

          ptr=send_msg;
          nbytes=strlen(ptr);
          nleft=nbytes;
          // Send menssege to next server
          while(nleft>0){
            nwritten=write(*fd_tcp,ptr,nleft);
            if(nwritten<=0){printf("Error: %s\n", strerror(errno));exit(1);}//error
              nleft-=nwritten;
              ptr+=nwritten;
            }
          nleft=nbytes;
        }else{

          printf("I AM NOT AVAILABLE...\n");
          //Reencaminha token para o próximo servidor
          ptr=read_msg;
          nbytes=strlen(ptr);
          nleft=nbytes;
          // Send menssege to next server
          while(nleft>0){
            nwritten=write(*fd_tcp,ptr,nleft);
            if(nwritten<=0){printf("Error: %s\n", strerror(errno));exit(1);}//error
              nleft-=nwritten;
              ptr+=nwritten;
            }
          nleft=nbytes;

        }
      }else{

        printf("RING NOT AVAILABLE\n");
        server->ring_available = 0;
        tokens(send_msg, "TOKEN", "I", server, "NO ID", "NO IP", "NO TCP");
        ptr=send_msg;
        nbytes=strlen(ptr);
        nleft=nbytes;
        // Send menssege to next server
        while(nleft>0){
          nwritten=write(*fd_tcp,ptr,nleft);
          if(nwritten<=0){printf("Error: %s\n", strerror(errno));exit(1);}//error
            nleft-=nwritten;
            ptr+=nwritten;
          }
        nleft=nbytes;
      }

    }
    if(strcmp("T", aux_type)==0)
    {
      if(server->own_ID == (unsigned int)atoi(id))
      {
        printf("SERVICE WAS TRANSFERED\n");
        if (server->is_leaving)
        {
          sprintf(id,"%d", server->next_server_ID);
          sprintf(bptcp,"%d", (int)ntohs(server->next_server_PTCP));

          tokens(send_msg, "TOKEN", "O", server, id, inet_ntoa(server->next_server_IP), bptcp);
          printf("LEAVE after confirmation %s \n", send_msg);
          ptr = send_msg;
          n = strlen(send_msg);
          while(n>0){if((nwritten=write(*fd_tcp,ptr,n))<=0){printf("Error: %s\n", strerror(errno));exit(1);}//error
            n-=nwritten; ptr+=nwritten;}

        }
      }else{


        //Reencaminha token para o próximo servidor
        ptr=read_msg;
        nbytes=strlen(ptr);
        nleft=nbytes;
        // Send menssege to next server
        while(nleft>0){
          nwritten=write(*fd_tcp,ptr,nleft);
          if(nwritten<=0){printf("Error: %s\n", strerror(errno));exit(1);}//error
            nleft-=nwritten;
            ptr+=nwritten;
          }
        nleft=nbytes;

      }
    }
    if(strcmp("I", aux_type)==0)
    {
      if(server->own_ID != (unsigned int)atoi(id))
      {
        server->ring_available = 0;

        ptr=read_msg;
        nbytes=strlen(ptr);
        nleft=nbytes;
        // Send menssege to next server
        while(nleft>0){
          nwritten=write(*fd_tcp,ptr,nleft);
          if(nwritten<=0){printf("Error: %s\n", strerror(errno));exit(1);}//error
            nleft-=nwritten;
            ptr+=nwritten;
          }
        nleft=nbytes;
      }

    }
    if(strcmp("D", aux_type)==0)
    {
      if(server->own_ID != (unsigned int)atoi(id))
      {
        server->ring_available = 1;

        ptr=read_msg;
        nbytes=strlen(ptr);
        nleft=nbytes;
        // Send menssege to next server
        while(nleft>0){
          nwritten=write(*fd_tcp,ptr,nleft);
          if(nwritten<=0){printf("Error: %s\n", strerror(errno));exit(1);}//error
            nleft-=nwritten;
            ptr+=nwritten;
          }
        nleft=nbytes;
      }else{
        printf("RING AVAILABLE CONFIRMED\n");
      }
    }
  }
  ////////////////TOKEN NEW START//////////////////////////
  if(strcmp("NEW_START", aux_str)==0)
  {
    printf("I got NEW_START token and i am the start server ...\n" );
    full_message("SET_START",server, send_msg);
    chat(send_msg, *fd_udp, *addr_udp, buffer);
    server->is_start = 1;
  }

}



/*  Cria uma string f_msg com os paramentros deste servidor para ser enviado para o seu sucessor */
/*  Creates a string with the desired token to send  */
/*  f_msg -> pointer to the resulting token string */
/*  msg -> specifies the token class (NEW, TOKEN or NEW_START)  */
/*  type -> specifies the token type (S, T, I, D and N, O)  */
void tokens(char *f_msg, char *msg, char *type, Server *own_server, char *aux_his_ID, char *aux_his_IP, char *aux_his_ptcp)
{
  int k = 0;
  char aux_own_ID[5]={0};
  char *aux_own_ip;
  char aux_own_ptcp[50]={0};

  memset(f_msg, 0, strlen(f_msg));

  sprintf(aux_own_ID,"%d", own_server->own_ID);
  aux_own_ip=inet_ntoa(own_server->own_server_IP);
  sprintf(aux_own_ptcp,"%d", ntohs(own_server->own_server_PTCP));

  if(strcmp("TOKEN", msg)==0)
  {
    if(strcmp("S", type)==0)
    {
      k = strlen(msg) + strlen(aux_own_ID) + strlen(type) + 4; //espaço, ponto e virgula, barra n e barra zero
      snprintf(f_msg, k, "%s %s;%s\n", msg, aux_own_ID, type);
    }
    if(strcmp("T", type)==0)
    {
      k = strlen(msg) + strlen(aux_own_ID) + strlen(type) + 4; //espaço, ponto e virgula, barra n e barra zero
      snprintf(f_msg, k, "%s %s;%s\n", msg, aux_his_ID, type);
    }
    if(strcmp("I", type)==0)
    {
      k = strlen(msg) + strlen(aux_own_ID) + strlen(type) + 4; //espaço, ponto e virgula, barra n e barra zero
      snprintf(f_msg, k, "%s %s;%s\n", msg, aux_own_ID, type);
    }
    if(strcmp("D", type)==0)
    {
      k = strlen(msg) + strlen(aux_own_ID) + strlen(type) + 4; //espaço, ponto e virgula, barra n e barra zero
      snprintf(f_msg, k, "%s %s;%s\n", msg, aux_own_ID, type);
    }
    if(strcmp("N", type)==0)
    {
      k = strlen(msg) + strlen(aux_own_ID) + strlen(type) + strlen(aux_his_ID) + strlen(aux_his_IP) + strlen(aux_his_ptcp) + 7; //espaço, ponto e virgulax4, barra n e barra zero
      snprintf(f_msg, k, "%s %s;%s;%s;%s;%s\n", msg, aux_own_ID, type, aux_his_ID, aux_his_IP, aux_his_ptcp);
    }
    if(strcmp("O", type)==0)
    {
      k = strlen(msg) + strlen(aux_own_ID) + strlen(type) + strlen(aux_his_ID) + strlen(aux_his_IP) + strlen(aux_his_ptcp) + 7; //espaço, ponto e virgulax4, barra n e barra zero
      snprintf(f_msg, k, "%s %s;%s;%s;%s;%s\n", msg, aux_own_ID, type, aux_his_ID, aux_his_IP, aux_his_ptcp);
    }
  }
  if(strcmp("NEW", msg)==0)
  {
    k = strlen(msg) + strlen(aux_own_ID) + strlen(aux_own_ip) + strlen(aux_own_ptcp) + 5; //espaço, ponto e virgulax2, barra n e barra zero
    snprintf(f_msg, k, "%s %s;%s;%s\n", msg, aux_own_ID, aux_own_ip, aux_own_ptcp);
  }
  if(strcmp("NEW_START", msg)==0)
  {
    k = strlen(msg) + 2; //espaço, ponto e virgulax2, barra n e barra zero
    snprintf(f_msg, k, "%s\n", msg);
  }
  printf("TOKEN SENT: %s",f_msg);
}
